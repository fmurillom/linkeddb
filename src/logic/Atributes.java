/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic;

/**
 *
 * @author fmuri
 */
public class Atributes implements java.io.Serializable{
    private String atrname;
    private Object atrvalue;
    private Object atrtype;
    private String atrrequired;
    private String atrforaneo;
    
    /**
     * Construtor para inicializar los los valores de los atributos de la clase.
     * @param atrname nombre del atributo
     * @param atrvalue valor del atributo
     * @param atrtype tipo del atributo
     * @param atrrequired si el atributo es requerido o no
     */
    public Atributes(String atrname, Object atrvalue, Object atrtype, String atrrequired, String atrforaneo){
        this.atrname = atrname;
        this.atrvalue = atrvalue;
        this.atrtype = atrtype;
        this.atrrequired = atrrequired;
        this.atrforaneo = atrforaneo;
    }

    public String getAtrname() {
        return this.atrname;
    }

    public Object getAtrvalue() {
        return this.atrvalue;
    }

    public Object getAtrtype() {
        return this.atrtype;
    }

    public String getAtrrequired() {
        return this.atrrequired;
    }
    
    public String getAtrforaneo(){
        return this.atrforaneo;
    }
    
    public void setAtrname(String atrName){
        this.atrname = atrName;
    }
    
    public void setAtrvalue(String atrValue){
        this.atrvalue = atrValue;
    }
    
    public void setAtrtype(String atrType){
        this.atrtype = atrType;
    }
    
    public void setArtrequired(String atrRequired){
        this.atrrequired = atrRequired;
    }
    
    public void setAtrforaneo(String foraneo){
        this.atrforaneo = foraneo;
    }
    
    
}
   