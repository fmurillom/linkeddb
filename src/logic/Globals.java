/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic;

import lists.DoubleList;

/**
 *Clase para almacenar las listas y poder acceder a ellas en distintos .java
 * @author fmuri
 */
public class Globals implements java.io.Serializable{
    private DoubleList JSONStores;
    private String selected_item;
    private String parent_item;
    private String error;
    
    
  
        static public Globals getLists(){
        return db_lists;
    }  
    
        
    /**
     * Instanciasion de la unica instancia que existira en ejecución.
     */
    static final private Globals db_lists = new Globals();

    public DoubleList getJSONStores() {
        return JSONStores;
    }

    public void setJSONStores(DoubleList JSONStores) {
        this.JSONStores = JSONStores;
    }

    public String getSelected_item() {
        return this.selected_item;
    }

    public void setSelected_item(String selected_item) {
        this.selected_item = selected_item;
    }

    public String getParent_item() {
        return this.parent_item;
    }

    public void setParent_item(String parent_item) {
        this.parent_item = parent_item;
    }

    public String getError() {
        return this.error;
    }

    public void setError(String error) {
        this.error = error;
    }
    
    
}
