/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import lists.DoubleList;
import logic.Globals;
import lists.DoubleNode;
import lists.Node;
import lists.SimpleList;
import logic.Atributes;
import org.json.simple.JSONObject;

/**
 *
 * @author fmuri
 */
public class rootlinkedDBController implements Initializable {
    
    
    @FXML TreeView trvw_stores;
    @FXML ContextMenu cntx_tree; 
    @FXML MenuItem add_store_opt;
    @FXML MenuItem show_itm_opt;
    @FXML MenuItem new_json_opt;
    @FXML MenuItem dlt_all_json_opt;
    @FXML MenuItem srch_atrib_opt;
    @FXML MenuItem upt_json_opt;
    @FXML MenuItem dlt_json_opt;
    @FXML MenuItem add_document_opt;
    @FXML TextArea txta_json;
    @FXML TextField txt_atribute;
    @FXML TextField txt_value;
    @FXML Button btn_search;
  
    
    @FXML
    /**
     * Metodo encargado de refrescar los valores del treeview tomanddolos de la lista de Stores.
     */
    private void refreshtree(){
        trvw_stores.setRoot(null);
        javafx.scene.Node img_linked = new ImageView(new Image(getClass().getResourceAsStream("/gui/images/folder.png")));
        TreeItem<String> treeDB = new TreeItem("LinkedDB", img_linked);
        DoubleNode stores_aux = Globals.getLists().getJSONStores().getHead();
        int list_size = Globals.getLists().getJSONStores().getSize();
        
        treeDB.setExpanded(true);
        for(int i = 0; i < list_size; i++){
        javafx.scene.Node img_db = new ImageView(new Image(getClass().getResourceAsStream("/gui/images/database.png")));
        TreeItem<String> document = new TreeItem(stores_aux.getName(), img_db);
        if(stores_aux.getDato2() == null){
            document.setExpanded(false);
        }else{
            document.setExpanded(true);
            Object searched = stores_aux.getName();
            int document_size = Globals.getLists().getJSONStores().search(searched).getDato2().getSize();
            DoubleNode documents_aux = stores_aux.getDato2().getHead();
            for (int j = 0; j < document_size; j++){
                    javafx.scene.Node img_docu = new ImageView(new Image(getClass().getResourceAsStream("/gui/images/schema.png")));
                    TreeItem<String> store = new TreeItem(documents_aux.getName(), img_docu);
                    document.getChildren().add(store);
                    if(documents_aux.getJsonList() == null){
                        store.setExpanded(false);
                    }else{
                        store.setExpanded(true);
                        Node json_aux = documents_aux.getJsonList().getHead();
                        for(int k = 0; k < documents_aux.getJsonList().getSize(); k++){
                            javafx.scene.Node img_json = new ImageView(new Image(getClass().getResourceAsStream("/gui/images/json.png")));
                            TreeItem<String> json = new TreeItem(json_aux.getJson().get("Name"), img_json);
                            store.getChildren().add(json);
                            json_aux = json_aux.getNext();
                        }
                    }
                    documents_aux = documents_aux.getNext();
             }
        }
        treeDB.getChildren().add(document);
        stores_aux = stores_aux.getNext();
    }
    trvw_stores.setRoot(treeDB);
}
    
    @FXML
    /**
     * Metodo para activar la ventana para agregar un nuevo Store.
     */
    private void add_store() throws IOException{
        add_store_opt.setDisable(true);
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/gui/add_store.fxml"));
        Parent root = fxmlLoader.load();
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setOpacity(1);
        stage.setTitle("Agregar Store");
        stage.setScene(new Scene(root));
        stage.showAndWait();
    }
    @FXML
    /**
     * Metodo para activar la ventana para agregar un documento en el Store seleccionado.
     */
    private void add_document() throws IOException{
        add_document_opt.setDisable(true);
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/gui/add_document.fxml"));
        Parent root = fxmlLoader.load();
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setOpacity(1);
        stage.setTitle("Agregar Documento");
        stage.setScene(new Scene(root));
        stage.showAndWait();
    } 
    
    @FXML
    /**
     * Metodo para activar la ventana para agregar objetos Json dentro de un documento seleccionado.
     */
    private void add_json() throws IOException{
        //nw_json_opt.setDisable(true);
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/gui/add_Json_object.fxml"));
        Parent root = fxmlLoader.load();
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setOpacity(1);
        stage.setTitle("Agregar JSON");
        stage.setScene(new Scene(root));
        stage.showAndWait();
    }
    
    @FXML
    /**
     * Metodo encargado de obtener el elemento seleccionado y activar las opciones del context menu dependiendo de lo seleccionado en el arbol.
     */
    private void activate_items(){
        DoubleNode aux_stores = Globals.getLists().getJSONStores().getHead();
        TreeItem selected = (TreeItem)trvw_stores.getSelectionModel().getSelectedItem();
        if(selected != null){
            Globals.getLists().setSelected_item(selected.getValue().toString());
            if(!selected.getValue().toString().equals("LinkedDB")){
            Globals.getLists().setParent_item(selected.getParent().getValue().toString());
            }
            if(selected.getValue().equals("LinkedDB")){
                add_store_opt.setDisable(false);
                if(Globals.getLists().getJSONStores().getHead() != null){
                    srch_atrib_opt.setDisable(false);
                }
            }
            if(Globals.getLists().getJSONStores().search(selected.getValue()) != null){
                add_document_opt.setDisable(false);
            }
            for (int i = 0; i < Globals.getLists().getJSONStores().getSize(); i++){
                if(aux_stores.getDato2().search(selected.getValue().toString()) != null){
                    new_json_opt.setDisable(false);
                    dlt_all_json_opt.setDisable(false);
                    break;
                }else{
                    aux_stores = aux_stores.getNext();
                }
            }
            if(selected.isLeaf() && selected.getValue().toString() != "LinkedDB"){
                dlt_json_opt.setDisable(false);
                upt_json_opt.setDisable(false);
                show_itm_opt.setDisable(false);
            }
        }
    }
    
    @FXML
    /**
     * Metodo para activar la ventana para mostrar los objetos Json con sus respectivos atributos.
     */
    private void show_json() throws IOException{
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/gui/show_json.fxml"));
        Parent root = fxmlLoader.load();
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setOpacity(1);
        stage.setTitle("Editar JSON");
        stage.setScene(new Scene(root));
        stage.showAndWait();
    }
    
    @FXML
    /**
     * Metodo para eliminar todos los objetos Json dentro de un documento.
     */
    private void delete_all_json(){
        DoubleNode aux_stores = Globals.getLists().getJSONStores().getHead();
        for(int i = 0; i < Globals.getLists().getJSONStores().getSize(); i++){
            DoubleNode document = aux_stores.getDato2().search(Globals.getLists().getSelected_item());
            if (document != null){
                document.setJsonList(null);
                break;
            }else{
                aux_stores = aux_stores.getNext();
            }
        }
    }
    
    @FXML
    /**
     * Metodo para elminar el objeto Json seleccionado.
     */
    private void delete_json(){
        SimpleList json_list = search_json_list(Globals.getLists().getSelected_item());
        if(json_list == null){
            System.out.println("No se encontro el Json");
        }else{
            json_list.deleteNode(Globals.getLists().getSelected_item());
        }
    }
    
    @FXML
    /**
     * Metodo para activar la ventana para editar los objetos JSON
     */
    private void edit_json() throws IOException{
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/gui/edit_json.fxml"));
        Parent root = fxmlLoader.load();
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setOpacity(1);
        stage.setTitle("Editar JSON");
        stage.setScene(new Scene(root));
        stage.showAndWait();
    }
    
    /**
     * Metodo para buscar un objeto JSON en todos los Stores.
     * @param searched ID del objeto JSON que se desea buscar.
     * @return 
     */
    private SimpleList search_json_list(String searched){
        DoubleNode aux_store = Globals.getLists().getJSONStores().getHead();
        for (int i = 0; i < Globals.getLists().getJSONStores().getSize(); i++){
            DoubleNode document_aux = aux_store.getDato2().getHead();
            for (int j = 0; j < aux_store.getDato2().getSize(); j++){
                SimpleList json_list = document_aux.getJsonList();
                if(json_list.search(searched) != null){
                    return json_list;
                }else{
                    document_aux = document_aux.getNext();
                }
            }
            aux_store = aux_store.getNext();
        }
        return null;
    }
    
    @FXML
    /**
     * Metodo utilizado para guardar todos los elementos de la lista de Stores en un archivo .ser
     */
    private void save_to_file() throws FileNotFoundException, IOException{
        try{
            FileOutputStream linkedDB = new FileOutputStream("DB.ser");
            ObjectOutputStream out = new ObjectOutputStream(linkedDB);
            out.writeObject(Globals.getLists().getJSONStores());
            out.close();
            linkedDB.close();
            System.out.println("Guardado");
        }catch(IOException i){
            i.printStackTrace();
        }
    }
    
    @FXML
    /**
     * Busca el archivo DB.ser y lo carga en memoria.
     */
    private void load_from_file() throws FileNotFoundException, IOException, ClassNotFoundException{
        DoubleList read = null;
        try{
            FileInputStream linkedDB = new FileInputStream("DB.ser");
            ObjectInputStream in = new ObjectInputStream(linkedDB);
            read = (DoubleList) in.readObject();
            in.close();
            linkedDB.close();
            Globals.getLists().setJSONStores(read);
        }catch(IOException i){
            i.printStackTrace();
            return;
        }catch(ClassNotFoundException c){
            System.out.println("No se encuentra la clase Globals");
            c.printStackTrace();
            return;
        }
    }
    
    @FXML
    /**
     * Metodo encargado de buscar en todos los Stores el atribut deseado atravez de nombre de atributo y valor asignado.
     */
    private void search_by_atributes(){
        String atr_name = txt_atribute.getText();
        String atr_value = txt_value.getText();
        String text = "";
        SimpleList agregados = new SimpleList();
        DoubleNode store_aux = Globals.getLists().getJSONStores().getHead();
        for(int i = 0; i < Globals.getLists().getJSONStores().getSize(); i++){
            DoubleNode docu_aux = store_aux.getDato2().getHead();
            for(int j = 0; j < store_aux.getDato2().getSize(); j++){
                Node json_aux = docu_aux.getJsonList().getHead();
                for(int k = 0; k < docu_aux.getJsonList().getSize(); k++){
                    Node atributes = docu_aux.getAtribute_list().getHead();
                    Atributes atribute = atributes.getAtribute();
                    for(int l = 0; l < docu_aux.getAtribute_list().getSize(); l++){
                        JSONObject json = json_aux.getJson();
                        Node agregar = new Node(json.get("Name"));

                        if(json.get(atr_name) != null && json.get(atr_name).equals(atr_value) && agregados.search_element(json.get("Name").toString()) == null){
                            text+= json.get("Name") + System.getProperty("line.separator");
                            agregados.add_element(agregar);
                        }
                        atributes = atributes.getNext();
                    }
                    json_aux = json_aux.getNext();
                }
                docu_aux = docu_aux.getNext();
            }
            store_aux = store_aux.getNext();
        }
        txta_json.setText(text);
    }

    @FXML
    /**
     * Metodo que activa los text box y el boton para buscar por atibuto.
     */
    private void activate_search(){
        txta_json.setDisable(false);
        txt_value.setDisable(false);
        txt_atribute.setDisable(false);
        btn_search.setDisable(false);
    }
    
    private void deactivate_search(){
        txta_json.setDisable(true);
        txt_value.setDisable(true);
        txt_atribute.setDisable(true);
        btn_search.setDisable(true);
    }
    
    @FXML
    /**
     * desactiva todos los items del context menu
     */
    private void disable_items(){
        add_store_opt.setDisable(true);
        show_itm_opt.setDisable(true);
        new_json_opt.setDisable(true);
        dlt_all_json_opt.setDisable(true);
        srch_atrib_opt.setDisable(true);
        upt_json_opt.setDisable(true);
        dlt_json_opt.setDisable(true);
        add_document_opt.setDisable(true);
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            load_from_file();
            refreshtree();
        } catch (ClassNotFoundException ex) {
            System.out.println("Error");
        } catch (IOException ex) {
            System.out.println("Error");
        } catch (Exception ex){
            TreeItem<String> treeDB = new TreeItem<>("LinkedDB");
            trvw_stores.setRoot(treeDB);
        }
    }    
    
}
