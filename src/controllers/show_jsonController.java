/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import lists.DoubleNode;
import lists.Node;
import logic.Atributes;
import logic.Globals;
import org.json.simple.JSONObject;

/**
 * FXML Controller class
 *
 * @author fmuri
 */
public class show_jsonController implements Initializable {

    @FXML TableColumn tblc_atribute;
    @FXML TableColumn tblc_value;
    @FXML TableColumn tblc_type;
    @FXML TableColumn tblc_required;    
    @FXML TableView<Atributes> table_atributes;
    @FXML Label lbl_name;
    
    private final ObservableList<Atributes> data =
        FXCollections.observableArrayList();
    
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        System.out.println(Globals.getLists().getParent_item());
        JSONObject json = search_json();
        DoubleNode document = search_document();
        Node atribute_node = document.getAtribute_list().getHead();
        for(int i = 0; i < document.getAtribute_list().getSize(); i++){
            Atributes atribute = atribute_node.getAtribute();
            String[] types = (String[]) json.get("Types");
            Atributes agregar = new Atributes(atribute.getAtrname(), 
                    json.get(atribute.getAtrname()), 
                    types[i], json.get("Required").toString(), json.get("Foraneo").toString());
            data.add(agregar);
            atribute_node = atribute_node.getNext();
        }
        lbl_name.setText(json.get("Name").toString());
        tblc_atribute.setCellValueFactory(
                        new PropertyValueFactory<Atributes, String>("atrname"));
        
        tblc_value.setCellValueFactory(
                        new PropertyValueFactory<Atributes, Object>("atrvalue"));
   
        tblc_type.setCellValueFactory(
                        new PropertyValueFactory<Atributes, Object>("atrtype"));
        
        tblc_required.setCellValueFactory(
                        new PropertyValueFactory<Atributes, String>("atrrequired"));
        table_atributes.setItems(data);
       
    }    
    
    /**
     * Metodo encargado de encontrar en memoria el objeto JSON seleccionado
     * @return Referencia al objeto JSON seleccionado.
     */
    private JSONObject search_json(){
        DoubleNode stores_aux = Globals.getLists().getJSONStores().getHead();
        for(int i = 0; i < Globals.getLists().getJSONStores().getSize(); i++){
            DoubleNode document = stores_aux.getDato2().getHead();
            for(int j = 0; j < stores_aux.getDato2().getSize(); j++){
                Node json_aux = document.getJsonList().getHead();
                for(int k = 0; k < document.getJsonList().getSize(); k++){
                    JSONObject json = json_aux.getJson();
                    if(json.get("Name").equals(Globals.getLists().getSelected_item()) && document.getName().equals(Globals.getLists().getParent_item())){
                        return json;
                    }else{
                        json_aux = json_aux.getNext();
                    }
                }
                document = document.getNext();
            }
            stores_aux = stores_aux.getNext();
        }
        return null;
    }
    /**
     * Busca el documento en el que se encuentra el objeto json selecionado.
     * @return referencia al documento donde se encuentra el objeto json seleccionado. null si no exisre un documento que contenga el objeto.
     */
    private DoubleNode search_document(){
        DoubleNode stores_aux = Globals.getLists().getJSONStores().getHead();
        for(int i = 0; i < Globals.getLists().getJSONStores().getSize(); i++){
            DoubleNode document = stores_aux.getDato2().getHead();
            for(int j = 0; j < stores_aux.getDato2().getSize(); j++){
                Node json_aux = document.getJsonList().getHead();
                for(int k = 0; k < document.getJsonList().getSize(); k++){
                    JSONObject json = json_aux.getJson();
                    if(json.get("Name").equals(Globals.getLists().getSelected_item()) && document.getName().equals(Globals.getLists().getParent_item())){
                        return document;
                    }else{
                        json_aux = json_aux.getNext();
                    }
                }
                document = document.getNext();
            }
            stores_aux = stores_aux.getNext();
        }
        return null;
    }
    
}
