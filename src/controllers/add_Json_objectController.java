/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.Modality;
import javafx.stage.Stage;
import lists.DoubleCircular;
import lists.DoubleList;
import lists.DoubleNode;
import lists.Node;
import lists.SimpleList;
import logic.Atributes;
import logic.Globals;
import org.json.simple.JSONObject;

/**
 * FXML Controller class
 *
 * @author fmuri
 */
public class add_Json_objectController implements Initializable {

    @FXML TableColumn tblc_atribute;
    @FXML TableColumn tblc_value;
    @FXML TableColumn tblc_type;
    @FXML TableColumn tblc_required;    
    @FXML ComboBox cb_foraneo;
    @FXML TableView<Atributes> table_atributes;
    @FXML TextField txt_json_name;
    @FXML Button btn_create;
    private int number;
    
    DoubleList prueba;
    
    private final ObservableList<Atributes> data =
        FXCollections.observableArrayList();
    
    private String[] new_data;
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        SimpleList atribute_list = get_atributes(Globals.getLists().getSelected_item()).getAtribute_list();
        Node atribute_aux = atribute_list.getHead();
        for(int i = 0; i < atribute_list.getSize(); i++){
            data.add(atribute_aux.getAtribute());
            atribute_aux = atribute_aux.getNext();
        }
        
        tblc_atribute.setCellValueFactory(
                        new PropertyValueFactory<Atributes, String>("atrname"));
        
        tblc_value.setCellValueFactory(
                        new PropertyValueFactory<Atributes, Object>("atrvalue"));
        tblc_value.setCellFactory(TextFieldTableCell.forTableColumn());
        tblc_value.setOnEditCommit(
            new EventHandler<CellEditEvent<Atributes, String>>() {
                @Override
                public void handle(CellEditEvent<Atributes, String> t) {
                    ((Atributes) t.getTableView().getItems().get(
                    t.getTablePosition().getRow())).setAtrvalue(t.getOldValue());
                    new_data[t.getTablePosition().getRow()] = t.getNewValue();
                }
            });
                
        tblc_type.setCellValueFactory(
                        new PropertyValueFactory<Atributes, Object>("atrtype"));
        
        tblc_required.setCellValueFactory(
                        new PropertyValueFactory<Atributes, String>("atrrequired"));
        table_atributes.setItems(data);
        
        number = table_atributes.getItems().size();
        this.new_data = new String[number];
        
    }    
    
    /**
     * Metodo encargado de obtener la lista de atributos del documento seleccionado.
     * @param document_name nombre del documento del que se obtendran los atributos.
     * @return retorna el documento que contendrá los atributos a utilizar.
     */
    private DoubleNode get_atributes(String document_name){
        DoubleNode store_aux = Globals.getLists().getJSONStores().getHead();
        for(int i = 0; i < Globals.getLists().getJSONStores().getSize(); i++){
            DoubleCircular document_aux = store_aux.getDato2();
            DoubleNode document = document_aux.search(document_name);
            if(document != null && store_aux.getName().equals(Globals.getLists().getParent_item())){
                return document;
                
            }else{
                store_aux = store_aux.getNext();
            }
        }
        return null;
    }
    
    @FXML
    /**
     * Método encargado de crear el objeto json y añadirlo a la lista de json dentro del documento seleccionado.
     */
    private void create_Json() throws IOException{
        if(!empty_field(new_data) && !txt_json_name.getText().equals("")){
            Stage stage = (Stage) btn_create.getScene().getWindow();
            JSONObject json = new JSONObject();
            json.put("Name", txt_json_name.getText());
            String[] types = new String[number];
            for(int i = 0; i < table_atributes.getItems().size(); i++){
                Object value;
                if(new_data[i] == null){
                    if(tblc_type.getCellData(i).equals("int")){
                        value = Integer.parseInt(tblc_value.getCellData(i).toString());
                    }else{
                        value = tblc_value.getCellData(i);
                    }
                json.put(tblc_atribute.getCellData(i), value);
                }else{
                    if(tblc_type.getCellData(i).equals("int")){
                        value = Integer.parseInt(new_data[i]);
                    }else{
                        value = new_data[i];
                    }
                    json.put(tblc_atribute.getCellData(i), value);
                }
                types[i] = tblc_type.getCellData(i).toString();
                json.put("Required", tblc_required.getCellData(i).toString());
            }
            json.put("Types", types);
            DoubleNode document = search_document();
            if(document.getJsonList() == null){
                document.setJsonList(new SimpleList());
            }
            Node jsobject = new Node(json);
            document.getJsonList().add_element(jsobject);
            stage.close();
        } else{
            Globals.getLists().setError("Required");
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/gui/error_window.fxml"));
            Parent root = fxmlLoader.load();
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setOpacity(1);
            stage.setTitle("Error");
            stage.setScene(new Scene(root));
            stage.showAndWait();
        }
    }
    /**
     * Retorna el documento en el cual se anadira la lista de objetos json.
     * @return retorna una referencia al documento seleccionado.
     */
    private DoubleNode search_document(){
        DoubleNode store_aux = Globals.getLists().getJSONStores().getHead();
        for(int i = 0; i < Globals.getLists().getJSONStores().getSize(); i++){
            DoubleNode document_found = store_aux.getDato2().search(Globals.getLists().getSelected_item());
            String parent = Globals.getLists().getParent_item();
            if(document_found != null && store_aux.getName().equals(Globals.getLists().getParent_item().toString())){
                return document_found;
            }else{
                store_aux = store_aux.getNext();
            }
        }
        return null;
    }
    
    /**
     * Funcion que detecta si existe algun campo requerido en blanco
     * @param data array de los valores nuevos introducidos en la tabla.
     * @return True si existe algun campo vacío, y retorna falso si todos los campos requeridos coneitnene un valor.
     */
    private boolean empty_field(String[] data){
        for (int i = 0; i < data.length; i++){
            if(data[i] == null && tblc_required.getCellData(i).equals("true")){
                return true;
            }
        }
        return false;
    }
    
}
