/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import logic.Globals;
import lists.DoubleNode;
import lists.Node;
import lists.SimpleList;
import logic.Atributes;

/**
 * FXML Controller class
 *
 * @author fmuri
 */
public class add_documentController implements Initializable {

    /**
     * Initializes the controller class.
     */
    
    @FXML ComboBox cb_bx_type;
    @FXML ComboBox cb_bx_foranea;
    @FXML Button btn_add;
    @FXML TextField txt_atr_name;
    @FXML TextField txt_atr_value;
    @FXML TableView<Atributes> table_atributes;
    @FXML CheckBox chbx_required;
    @FXML TextField txt_docu_name;
    @FXML TableColumn tblc_atribute;
    @FXML TableColumn tblc_value;
    @FXML TableColumn tblc_type;
    @FXML TableColumn tblc_required;
    @FXML Button btn_create;
    
    
    private final ObservableList<Atributes> data =
            FXCollections.observableArrayList();
    
    private SimpleList atributes_list;
    
    
    @FXML
    /**
     * Inserta el atributo generado en la table y también en la lista de atributos contenida para el documento que será creado.
     */
    private void insert_into_table() throws IOException{
        String required;
        Object value = null;
        boolean correct_type = true;
        String foraneo = "";
        
        if(this.txt_atr_name.getText().equals("") || this.cb_bx_type.getValue() == null){
            Globals.getLists().setError("Vacios");
            Globals.getLists().setError("Required");
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/gui/error_window.fxml"));
            Parent root = fxmlLoader.load();
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setOpacity(1);
            stage.setTitle("Error");
            stage.setScene(new Scene(root));
            stage.showAndWait();
        } else{
            if (chbx_required.isSelected()){
            required = "true";
            } else{
                required = "false";
            }
            if(cb_bx_type.getValue().equals("int")){
                try{
                Integer.parseInt(txt_atr_value.getText());
                correct_type = true;
                } catch (NumberFormatException e){
                    correct_type = false;
                }
            }
            if(correct_type){
                if(cb_bx_foranea.getValue() != null){
                    foraneo = cb_bx_foranea.getValue().toString();
                }
                Atributes agregar = new Atributes(txt_atr_name.getText(), txt_atr_value.getText(), cb_bx_type.getValue(), required, foraneo);
                data.add(agregar);
                table_atributes.refresh();
                Atributes atributes = new Atributes(txt_atr_name.getText(), txt_atr_value.getText(), cb_bx_type.getValue(), required, foraneo);
                Node atribute = new Node(null, atributes);
                this.atributes_list.add_element(atribute);
                clear_fields();
            }else {
                Globals.getLists().setError("Type");
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/gui/error_window.fxml"));
                Parent root = fxmlLoader.load();
                Stage stage = new Stage();
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.setOpacity(1);
                stage.setTitle("Error");
                stage.setScene(new Scene(root));
                stage.showAndWait();
            }
        }
    }
    
    @FXML
    /**
     * Metodo utilizado por el boton crear elemento, verifica que este contenga un nombre, y su lista de atributos no este vacia.
     */
    private void create_document() throws IOException{
        if(!this.txt_docu_name.getText().equals("") && this.atributes_list.getHead() != null){
            Stage stage = (Stage) btn_create.getScene().getWindow();
            DoubleNode document = new DoubleNode(txt_docu_name.getText(), this.atributes_list);
            document.setJsonList(new SimpleList());
            DoubleNode aux = Globals.getLists().getJSONStores().search(Globals.getLists().getSelected_item());
            aux.getDato2().add_element(document);
            stage.close();
        }else{
            Globals.getLists().setError("Docu Name");
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/gui/error_window.fxml"));
            Parent root = fxmlLoader.load();
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setOpacity(1);
            stage.setTitle("Error");
            stage.setScene(new Scene(root));
            stage.showAndWait();
        }
    }
    
    /**
     * Metodo utilizado para limpiar los campos de los tributos a generar.
     */
    private void clear_fields(){
        txt_atr_name.clear();
        txt_atr_value.clear();
        chbx_required.setSelected(false);
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.atributes_list = new SimpleList();
        
        cb_bx_type.getItems().addAll("int", "String");
                
        tblc_atribute.setCellValueFactory(
                        new PropertyValueFactory<Atributes, String>("atrname"));
        
        tblc_value.setCellValueFactory(
                        new PropertyValueFactory<Atributes, Object>("atrvalue"));
        
        tblc_type.setCellValueFactory(
                        new PropertyValueFactory<Atributes, Object>("atrtype"));
        
        tblc_required.setCellValueFactory(
                        new PropertyValueFactory<Atributes, String>("atrrequired"));
        table_atributes.setItems(data);
        
        SimpleList docu_list = get_all_documents();
        Node docu_aux = docu_list.getHead();
        for(int i = 0; i < docu_list.getSize(); i++){
            cb_bx_foranea.getItems().add(docu_aux.getDato());
            docu_aux = docu_aux.getNext();
        }
        
    }    
    
    /**
     * Lista de todos los documentos existentes en todas las Stores y luego la retorna.
     * @return Lista conteniendo todos los documentos.
     */
    private SimpleList get_all_documents(){
        SimpleList docu_list = new SimpleList();
        DoubleNode stores_aux =  Globals.getLists().getJSONStores().getHead();
        for(int i = 0; i < Globals.getLists().getJSONStores().getSize(); i++){
            DoubleNode docu_aux = stores_aux.getDato2().getHead();
            for(int j = 0; j < stores_aux.getDato2().getSize(); j++){
                Node docu_add = new Node(docu_aux.getName());
                docu_list.add_element(docu_add);
                docu_aux = docu_aux.getNext();
            }
            stores_aux = stores_aux.getNext();
        }
        return docu_list;
    }
}
