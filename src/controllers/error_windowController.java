/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import logic.Globals;

/**
 * FXML Controller class
 *
 * @author fmuri
 */

public class error_windowController implements Initializable {

    /**
     * Initializes the controller class.
     */
    
    @FXML Label lbl_error;
    @FXML Button btn_exit;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        if(Globals.getLists().getError().equals("Store")){
            lbl_error.setText("Se debe ingresar un nombre para el Store a crear.");
        }
        if(Globals.getLists().getError().equals("Vacios")){
            lbl_error.setText("Elementos necesarios se encuentran vacios.");
        }
        if(Globals.getLists().getError().equals("Type")){
            lbl_error.setText("El tipo seleccionado y el valor insertado no coinciden.");
        }
        if(Globals.getLists().getError().equals("Required")){
            lbl_error.setText("No se le asigno valor a los atributos requeridos.");
        }
        if(Globals.getLists().getError().equals("Docu Name")){
            lbl_error.setText("Se debe Ingresar un nombre para el documento.");
            
        }
    }
    

    @FXML
    private void close_window(){
        Stage stage = (Stage) btn_exit.getScene().getWindow();
        stage.close();
    }
    
}
