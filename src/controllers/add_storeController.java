/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import logic.Globals;
import lists.DoubleCircular;
import lists.DoubleNode;

/**
 * FXML Controller class
 *
 * @author fmuri
 */
public class add_storeController implements Initializable {

    /**
     * Initializes the controller class.
     */
    
    @FXML Button btn_agregar;
    @FXML TextField str_txt;
    
    @FXML
    /**
     * Metodo que tomara el nombre especificado para el store y la añadira a la lista de Stores.
     */
    private void btn_add() throws IOException{
        if(!str_txt.getText().equals("")){
            Stage stage = (Stage) btn_agregar.getScene().getWindow();
            DoubleNode store_name = new DoubleNode(str_txt.getText());
            DoubleCircular document_list = new DoubleCircular();
            store_name.setDato2(document_list);
            Globals.getLists().getJSONStores().add_element(store_name);
            stage.close();
        } else{
            Globals.getLists().setError("Store");
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/gui/error_window.fxml"));
            Parent root = fxmlLoader.load();
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setOpacity(1);
            stage.setTitle("Error");
            stage.setScene(new Scene(root));
            stage.showAndWait();
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
