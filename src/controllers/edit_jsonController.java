/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.Stage;
import lists.DoubleCircular;
import lists.DoubleNode;
import lists.Node;
import lists.SimpleList;
import logic.Atributes;
import logic.Globals;
import org.json.simple.JSONObject;

/**
 * FXML Controller class
 *
 * @author fmuri
 */
public class edit_jsonController implements Initializable {

    @FXML TableColumn tblc_atribute;
    @FXML TableColumn tblc_value;
    @FXML TableColumn tblc_type;
    @FXML TableColumn tblc_required;    
    @FXML TableView<Atributes> table_atributes;
    @FXML Button btn_edit;
    @FXML Label lbl_jsoname;
    private JSONObject json_edit;
    
    private final ObservableList<Atributes> data =
        FXCollections.observableArrayList();
    
    private String[] new_data;
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        String value;
        SimpleList atribute_list = get_json_document(Globals.getLists().getSelected_item()).getAtribute_list();
        Node atribute_aux = atribute_list.getHead();
        this.json_edit = search_json_list(Globals.getLists().getSelected_item());
        String[] array = (String[]) this.json_edit.get("Types");
        lbl_jsoname.setText(this.json_edit.get("Name").toString());
        for(int i = 0; i < atribute_list.getSize(); i++){
            if(array[i].equals("int")){
                value = Integer.toString(Integer.parseInt(this.json_edit.get(atribute_aux.getAtribute().getAtrname()).toString()));
            }else{
                value = this.json_edit.get(atribute_aux.getAtribute().getAtrname()).toString();
            }
            Atributes aux_atribute = new Atributes(atribute_aux.getAtribute().getAtrname(), 
                    value, atribute_aux.getAtribute().getAtrtype(), 
                    atribute_aux.getAtribute().getAtrrequired(), atribute_aux.getAtribute().getAtrforaneo());
            this.data.add(aux_atribute);
            atribute_aux = atribute_aux.getNext();
        }
        tblc_atribute.setCellValueFactory(
                        new PropertyValueFactory<Atributes, String>("atrname"));
        
        tblc_value.setCellValueFactory(
                        new PropertyValueFactory<Atributes, Object>("atrvalue"));
        tblc_value.setCellFactory(TextFieldTableCell.forTableColumn());
        tblc_value.setOnEditCommit(
            new EventHandler<CellEditEvent<Atributes, String>>() {
                @Override
                public void handle(CellEditEvent<Atributes, String> t) {
                    ((Atributes) t.getTableView().getItems().get(
                    t.getTablePosition().getRow())).setAtrvalue(t.getOldValue());
                    new_data[t.getTablePosition().getRow()] = t.getNewValue();
                }
            });
                
        tblc_type.setCellValueFactory(
                        new PropertyValueFactory<Atributes, Object>("atrtype"));
        
        tblc_required.setCellValueFactory(
                        new PropertyValueFactory<Atributes, String>("atrrequired"));
        table_atributes.setItems(data);
        
        int number = table_atributes.getItems().size();
        this.new_data = new String[number];
    }    
    
    /**
     * Buscara en la lista de documentos al documento cuyo nombre se pasara por parametro.
     * @param document_name nombre del documento a buscar
     * @return retorna una referencia al documento buscado.
     */
    private DoubleNode search_document(String document_name){
        DoubleNode store_aux = Globals.getLists().getJSONStores().getHead();
        for(int i = 0; i < Globals.getLists().getJSONStores().getSize(); i++){
            DoubleCircular document_aux = store_aux.getDato2();
            DoubleNode document = document_aux.search(document_name);
            if(document == null){
                store_aux = store_aux.getNext();
            }else{
                return document;
            }
        }
        return null;
    }
    
    @FXML
    /**
     * Metodo que editara el valor de la llave brindada por ell nombre del atributo que fue editado en la tabla.
     */
    private void update_Json(){
       for(int i = 0; i < table_atributes.getItems().size(); i++){
           if(new_data[i] == null){
               this.json_edit.put(tblc_atribute.getCellData(i).toString(), tblc_value.getCellData(i).toString());
           }else{
               this.json_edit.put(tblc_atribute.getCellData(i).toString(), new_data[i]);
           }
       }
       Stage stage = (Stage) btn_edit.getScene().getWindow();
       stage.close();
    }
    
    /**
     * Metodo para buscar un objeto JSON dentro de la lista de objetos JSON en el documento seleccionado.
     * @param searched ID del documento JSON a buscar.
     * @return referncia al objeto json que se desea editar, null si el documento json no existe.
     */
    private JSONObject search_json_list(String searched){
        DoubleNode aux_store = Globals.getLists().getJSONStores().getHead();
        for (int i = 0; i < Globals.getLists().getJSONStores().getSize(); i++){
            DoubleNode document_aux = aux_store.getDato2().getHead();
            for (int j = 0; j < aux_store.getDato2().getSize(); j++){
                SimpleList json_list = document_aux.getJsonList();
                if(json_list.search(searched) != null){
                    return json_list.search(searched).getJson();
                }else{
                    document_aux = document_aux.getNext();
                }
            }
            aux_store = aux_store.getNext();
        }
        return null;
    }
    
    /**
     * Obtiene el documento al que pertenece el objeto JSON seleccionado.
     * @param searched Nombre del objeto json seleccionado
     * @return retorna una referencia al documento que contiene el objeto json seleccioando.
     */
    private DoubleNode get_json_document(String searched){
        DoubleNode aux_store = Globals.getLists().getJSONStores().getHead();
        for (int i = 0; i < Globals.getLists().getJSONStores().getSize(); i++){
            DoubleNode document_aux = aux_store.getDato2().getHead();
            for (int j = 0; j < aux_store.getDato2().getSize(); j++){
                SimpleList json_list = document_aux.getJsonList();
                if(json_list.search(searched) != null){
                    return document_aux;
                }else{
                    document_aux = document_aux.getNext();
                }
            }
            aux_store = aux_store.getNext();
        }
        return null;
    }
}
