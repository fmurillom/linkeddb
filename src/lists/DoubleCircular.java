/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lists;

/**
 *
 * @author fmuri
 */
public class DoubleCircular extends DoubleList implements java.io.Serializable{
    
    /**
     * Constructor utilizado para inicializar los atributos de la llista doble.
     */
    public DoubleCircular(){
        this.head = null;
        this.last = null;
        this.size = 0;
    }
    
    
    /**
     * Se encarga de agregar el nodo en la lista
     * @param nodo Nodo a agregar en la lista
     */
    public DoubleCircular(DoubleNode nodo){
        this.add_first(nodo);
    }
    
    @Override
    /**
     * Metodo encargado de sobreescribir el meodo de agregar al inicio de la lista heredado de la clase lista doble
     */
    protected void add_first(DoubleNode nodo){
        nodo.setNext(this.head);
        if (!this.isEmpty()){
            this.head.setBefore(nodo);
        }
        this.head = nodo;
        this.last = nodo;
        nodo.setBefore(this.last);
        this.size++;
    }
    
    @Override
    /**
     * Metodo encargado de sobreescribir el meodo de agregar al final de la lista heredado de la clase lista doble
     */
    protected void add_last(DoubleNode nodo){
        this.last.setNext(nodo);
        nodo.setBefore(this.last);
        this.last = nodo;
        this.last.setNext(this.head);
        this.head.setBefore(this.last);
        this.size++;
    }
    
}
