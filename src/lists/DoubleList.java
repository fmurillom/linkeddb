/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lists;

/**
 *
 * @author fmuri
 */
public class DoubleList implements java.io.Serializable{
    
    protected DoubleNode head;
    protected DoubleNode last;
    protected int size = 0;
    
    /**
     * Constructor para inicializar la lista doble.
     */
    public DoubleList(){
        this.head = null;
        this.last = null;
        this.size = 0;
    }
    
    /**
     * Constructor para inicializar la instancia brindando un nodo para insertar al inicio.
     * @param nodo 
     */
    public DoubleList(DoubleNode nodo){
        this.head = nodo;
        this.last = nodo;
        this.size++;
    }

    public DoubleNode getHead() {
        return this.head;
    }

    public DoubleNode getLast() {
        return this.last;
    }
    
    
    /**
     * Metodo protegido para insertar al inicio de una lista.
     * @param nodo nodo a insertar
     */
    protected void add_first(DoubleNode nodo){
        nodo.setNext(this.head);
        if (!this.isEmpty()){
            this.head.setBefore(nodo);
        }
        this.head = nodo;
        this.last = nodo;
        this.size++;
    }
    /**
     * Metodo protegido para insertar un elemento al final de la lista.
     * @param nodo nodo a insertar
     */
    protected void add_last(DoubleNode nodo){
        this.last.setNext(nodo);
        nodo.setBefore(this.last);
        this.last = nodo;
        this.size++;
        
    }
    
    /**
     * Funcion auxiliar para insertar un elemento dependiendo del tamaño que tenga la lista.
     * @param nodo nodo a insertar
     */
    public void add_element(DoubleNode nodo){
        if (this.head == null){
            this.add_first(nodo);
            this.last = nodo;
        }
        else{
            this.add_last(nodo);
        }
    }
    
    /**
     * Funcion para buscar por valor un nodo dentro de la lista.
     * @param element Elemento a buscar
     * @return referencia al nodo que contiene el valor buscado.
     */
    public DoubleNode search(Object element){
        DoubleNode aux = this.head;
        for (int i = 0; i < this.size; i++){
            if(aux.getName().equals(element)){
                return aux;
            }
            else{
                aux = aux.getNext();
            }
        }
        return null;
    }
    
    /**
     * Metodo para editar el valor de un nodo
     * @param old_data valor del nodo a editar
     * @param new_data nuevo valor del nodo
     * @return referencia al nodo editado.
     */
    public DoubleNode editNode(Object old_data, Object new_data){
        DoubleNode aux = this.search(old_data);
        aux.setName(new_data);
        return aux;
    }
    

    public int getSize() {
        return this.size;
    }
    
    /**
     * Verifica si la lista se encuentra vacía.
     * @return True si la lista esta vacía y false si no se encuentra vacía.
     */
    public boolean isEmpty(){
        return this.head == null && this.last == null;
    }
}
