/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lists;

import logic.Atributes;
import org.json.simple.JSONObject;

/**
 *
 * @author fmuri
 */
public class Node implements java.io.Serializable{
    private Node next;
    private Object dato;
    private Atributes atribute;
    private JSONObject json;
    
    /**
 * Constructor para inicializar los atributos de la clase
 * @author fmuri
 */
    public Node(){
        this.next = null;
        this.dato = null;
        this.atribute = null;
        this.json = null;
    }
    /**
 * Constructor para inicializar el atributo dato con un valor especifico.
 * @author fmuri
 */
    public Node(Object dato){
        this.dato = dato;
        this.atribute = null;
    }
    /**
    * Constructor para inicializar el atributo Json con un valor especifico
    * @author fmuri
    * @param json documento Json con el que se inicializa el atributo.
    */
    public Node(JSONObject json){
        this.json = json;
        this.atribute = null;
        this.next = null;
        this.dato = null;
    }
    /**
     * Constructor para inicializar un nodo agregandole un valor al nombre 
     * @param dato1
     * @param dato2 
     */
    public Node(Object dato1, Atributes dato2){
        this.dato = dato1;
        this.atribute = dato2;
    }

    public Object getDato(){
        return this.dato;
    }
     
    public Node getNext(){
        return this.next;
    }
    
    public void setNext(Node nodo){
        this.next = nodo;
    }
    
    public void setData(Object new_data){
        this.dato = new_data;
    }

    public Atributes getAtribute() {
        return this.atribute;
    }

    public void setDato2(Atributes dato2) {
        this.atribute = dato2;
    }

    public JSONObject getJson() {
        return this.json;
    }

    public void setJson(JSONObject json) {
        this.json = json;
    }
    
}
