/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lists;

/**
 *
 * @author fmuri
 */
public class DoubleNode implements java.io.Serializable{
    private DoubleNode next;
    private DoubleNode before;
    private Object name;
    private DoubleCircular dato2;
    private SimpleList JsonList;
    private SimpleList atribute_list;
    
    
    /**
     * Constructor para inicilizar los valores de los atributos.
     */
    public DoubleNode(){
        this.before = null;
        this.next = null;
        this.name = null;
        this.dato2 = null;
        this.atribute_list = null;
    }
    
    /**
     * Constructor para inicializar la instancia dandole un nombre al nodo
     * @param name nombre que tendra el nodo
     */
    public DoubleNode(Object name){
        this.before = null;
        this.next = null;
        this.name = name;
    }
    
    /**
     * Constructor para inicilizar brindadnod el nombre y la lista de atributos.
     * @param name
     * @param atribute_list 
     */
    public DoubleNode(Object name, SimpleList atribute_list){
        this.name = name;
        this.atribute_list = atribute_list;
    }
    
    public DoubleNode getBefore(){
        return this.before;
    }
    
    public void setBefore(DoubleNode nodo){
        this.before = nodo;
    }
    
    public void setNext(DoubleNode nodo){
        this.next = nodo;
    }
    
    public DoubleNode getNext(){
        return this.next;
    }
    
    public Object getName(){
        return this.name;
    }
    
    public void setName(Object data){
        this.name = data;
    }

    public DoubleCircular getDato2() {
        return this.dato2;
    }

    public void setDato2(DoubleCircular dato2) {
        this.dato2 = dato2;
    }

    public SimpleList getAtribute_list() {
        return this.atribute_list;
    }

    public void setAtribute_list(SimpleList atribute_list) {
        this.atribute_list = atribute_list;
    }

    public SimpleList getJsonList() {
        return this.JsonList;
    }

    public void setJsonList(SimpleList JsonList) {
        this.JsonList = JsonList;
    }
    
    
    
}
